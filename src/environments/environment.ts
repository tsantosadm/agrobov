// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBvYkqJD-pB66aGcrEwEN5Uu3iKaXpxQp8',
    authDomain: 'agrobov-9b233.firebaseapp.com',
    databaseURL: 'https://agrobov-9b233.firebaseio.com',
    projectId: 'agrobov-9b233',
    storageBucket: 'agrobov-9b233.appspot.com',
    messagingSenderId: '957455516327',
    appId: '1:957455516327:web:430d3b46f82a4bd39fd182',
    measurementId: 'G-RG6B2RKYHC'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
