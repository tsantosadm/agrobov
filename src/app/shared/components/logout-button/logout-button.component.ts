import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { MenuController, NavController, } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';


@Component({
  selector: 'app-logout-button',
  template: `
    <ion-buttons>
      <ion-button (click) = "logout()">
        <ion-icon name="exit" slot="icon-only"></ion-icon>
      </ion-button>
    </ion-buttons>`,

})
export class LogoutButtonComponent implements OnInit {
  @Input() menu: string;

  constructor(
    private authServive: AuthService,
    private menuContrl: MenuController,
    private navContrl: NavController,
    private overLayService: OverlayService
  ) { }

  async ngOnInit(): Promise<void> {
    if (!await this.menuContrl.isEnabled(this.menu)) {
      this.menuContrl.enable(true, this.menu);
    }
  }

  async logout(): Promise<void> {
    await this.overLayService.alert({
      message: 'Tem certeza que Deseja Sair? ',
      buttons: [
        {
          text: 'Sim',
          handler: async () => {
            await this.authServive.logout();
            await this.menuContrl.enable(false, this.menu);
            this.navContrl.navigateRoot('/login');

          }
        },
        'Não'
      ]
    });
  }

}
