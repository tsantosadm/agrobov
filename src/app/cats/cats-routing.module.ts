import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../core/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      { path: 'map',
        loadChildren: './pages/map/map.module#MapPageModule' },
      { path: 'aves',
        loadChildren: './pages/aves-list/aves-list.module#AvesListPageModule'
      },
      { path: 'boi',
        loadChildren: './pages/boi-list/boi-list.module#BoiListPageModule'
      },
      { path: 'cavalo',
        loadChildren: './pages/cavalo-list/cavalo-list.module#CavaloListPageModule'
      },
      { path: 'porco',
        loadChildren: './pages/porco-list/porco-list.module#PorcoListPageModule'
      },
      {
        path: '',
        loadChildren: './pages/cats-list/cats-list.module#CatsListPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatsRoutingModule { }
