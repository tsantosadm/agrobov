import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatsListPage } from './cats-list.page';

describe('CatsListPage', () => {
  let component: CatsListPage;
  let fixture: ComponentFixture<CatsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatsListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
