import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { CatsListPage } from './cats-list.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: CatsListPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CatsListPage]
})
export class CatsListPageModule {}
