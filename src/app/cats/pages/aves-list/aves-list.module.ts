import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { AvesListPage } from './aves-list.page';


const routes: Routes = [
  {
    path: '',
    component: AvesListPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AvesListPage]
})
export class AvesListPageModule {}
