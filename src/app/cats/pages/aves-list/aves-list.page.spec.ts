import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvesListPage } from './aves-list.page';

describe('AvesListPage', () => {
  let component: AvesListPage;
  let fixture: ComponentFixture<AvesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvesListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
