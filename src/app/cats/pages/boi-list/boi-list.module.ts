import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { BoiListPage } from './boi-list.page';


const routes: Routes = [
  {
    path: '',
    component: BoiListPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BoiListPage]
})
export class BoiListPageModule {}
