import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoiListPage } from './boi-list.page';

describe('BoiListPage', () => {
  let component: BoiListPage;
  let fixture: ComponentFixture<BoiListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoiListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoiListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
