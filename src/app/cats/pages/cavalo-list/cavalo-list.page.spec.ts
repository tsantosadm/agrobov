import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CavaloListPage } from './cavalo-list.page';

describe('CavaloListPage', () => {
  let component: CavaloListPage;
  let fixture: ComponentFixture<CavaloListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CavaloListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CavaloListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
