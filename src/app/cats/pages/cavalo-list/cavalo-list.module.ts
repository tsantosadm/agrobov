import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SharedModule } from 'src/app/shared/shared.module';
import { CavaloListPage } from './cavalo-list.page';


const routes: Routes = [
  {
    path: '',
    component: CavaloListPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CavaloListPage]
})
export class CavaloListPageModule {}
