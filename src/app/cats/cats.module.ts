import { NgModule } from '@angular/core';

import { CatsRoutingModule } from './cats-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CatsRoutingModule
  ]
})
export class CatsModule { }
